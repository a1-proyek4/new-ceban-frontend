import { CebanDev2Page } from './app.po';

describe('ceban-dev2 App', () => {
  let page: CebanDev2Page;

  beforeEach(() => {
    page = new CebanDev2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
