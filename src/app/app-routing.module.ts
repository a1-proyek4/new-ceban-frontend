import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './components/pages/component-for-home-page/home-page/home-page.component';
import { CategoryPageComponent } from './components/pages/component-for-category-page/category-page/category-page.component';
import { ProductPageComponent } from './components/pages/component-for-product-page/product-page/product-page.component';
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: HomePageComponent },
  { path: 'products-by-category/:id',  component: CategoryPageComponent },
  { path: 'products',  component: ProductPageComponent }
 ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}