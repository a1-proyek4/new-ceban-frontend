import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../services/message.service';
import { Product } from '../../../../model/product';
import { Category } from '../../../../model/category';
import { ProductsService } from '../../../../services/products.service';
import { ActivatedRoute, Params }   from '@angular/router';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.css'],
  providers: [ProductsService]
})
export class CategoryPageComponent implements OnInit {

  private keyCategory="_category_load_";
  private key="_products_by_category_";
  private productsByCategory: Product[];
  private errorMessage: string;
  private categories: Category[];

  constructor(private prodService : ProductsService, private msgService: MessageService,private route: ActivatedRoute) { 
    //  this.categories=JSON.parse(localStorage.getItem(this.keyCategory));
  }

  ngOnInit() {
  	if(localStorage.getItem(this.key)===null || localStorage.getItem(this.key) === undefined) {
  		this.getProductsByCategories();
  	}else  
        {
          this.productsByCategory=JSON.parse(localStorage.getItem(this.key));
          this.getProductsByCategories();
          console.log("ISI CATEGORY"+localStorage.getItem(this.key));
        }
  }

  getProductsByCategories(): void {
    this.route.params
       .switchMap((param: Params)=>this.prodService.getProductsByCategory(+param['id']))
       .subscribe(cat =>{this.productsByCategory=cat;localStorage.setItem(this.key, JSON.stringify(cat));}, err=>console.log(err));
  	 }

 


}
