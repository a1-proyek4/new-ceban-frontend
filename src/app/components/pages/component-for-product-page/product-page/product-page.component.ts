import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../services/message.service';
import { Product } from '../../../../model/product';
import { ProductsService } from '../../../../services/products.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css'],
  providers: [ProductsService]
})
export class ProductPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
