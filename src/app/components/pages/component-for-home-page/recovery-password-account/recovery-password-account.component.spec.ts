import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveryPasswordAccountComponent } from './recovery-password-account.component';

describe('RecoveryPasswordAccountComponent', () => {
  let component: RecoveryPasswordAccountComponent;
  let fixture: ComponentFixture<RecoveryPasswordAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveryPasswordAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryPasswordAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
