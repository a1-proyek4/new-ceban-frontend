import { Component, OnInit } from '@angular/core';
import { NewProductsService } from '../../../../services/new-products.service';
import { MessageService} from '../../../../services/message.service';
import { Product} from '../../../../model/product';

@Component({
  selector: 'app-new-products',
  templateUrl: './new-products.component.html',
  styleUrls: ['./new-products.component.css'],
  providers: [NewProductsService]
})
export class NewProductsComponent implements OnInit {

  private newProducts: Product[];
  private keyNewProducts="_new_products_";
  constructor(private newProductsService : NewProductsService) { }

  ngOnInit() {
    if(localStorage.getItem(this.keyNewProducts) === null || localStorage.getItem(this.keyNewProducts) === undefined) {
      this.getNewProducts();
    }else{
       console.log("FETCH FROM localStorage"+localStorage.getItem(this.keyNewProducts));
      this.newProducts=JSON.parse(localStorage.getItem(this.keyNewProducts));
      //this.getNewProducts();
      console.log("ISI DATA"+this.newProducts);
    }
  }

  getNewProducts(): void {
    console.log("FETCH FETCH SERVER");
    this.newProductsService.getNewProducts()
                            .subscribe(NewProduct => {this.newProducts=NewProduct; localStorage.setItem(this.keyNewProducts, JSON.stringify(NewProduct));}, err=> console.log(err));
  }
}
