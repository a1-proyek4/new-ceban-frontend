import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../../../services/categories.service';
import { MessageService } from '../../../../services/message.service';
import { Category } from '../../../../model/category';
import { Observable} from 'rxjs/Observable';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [CategoriesService]
})
export class CategoryComponent implements OnInit {

  private categories: Category[];
  private errorMessage: string;
  private key="_category_load_";
  constructor(private catService : CategoriesService, private msgService: MessageService) { }

  ngOnInit() {
  	if(localStorage.getItem(this.key)===null || localStorage.getItem(this.key) === undefined) {
  		this.getCategories();
  	}else  
        {

          this.categories=JSON.parse(localStorage.getItem(this.key));
          this.getCategories();
          console.log("ISI CATEGORY"+localStorage.getItem(this.key));
        }
  }

  getCategories(): void {
  	this.catService.getCategories().subscribe(cat =>{this.categories=cat;localStorage.setItem(this.key, JSON.stringify(cat));} , er => console.log(er));	
  }

  sendId(id: string) {
    console.log("clicked: "+id)
    this.msgService.sendMessage(id);
  }

}
