import { Component, AfterViewInit,ElementRef, OnInit,AfterViewChecked, AfterContentInit} from '@angular/core';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css', '../../../../../assets/css/bootstrap.css', '../../../../../assets/css/font-awesome.css', '../../../../../assets/css/styles2.css','../../../../../assets/css/mystyles.css']
})
export class HomePageComponent implements AfterViewInit, OnInit {

	loadAPI: Promise<any>;
	js=['jquery.js','bootstrap.js','icheck.js','ionrangeslider.js','jqzoom.js','card-payment.js','owl-carousel.js','magnific.js','custom.js'];
  	constructor(private elementRef:ElementRef) { 
	  // this.loadScript();
	}

  ngOnInit() {
  	// this.loadScript();
  	// this.loadAPI = new Promise((resolve) => {
   //          console.log('resolving promise...');
   //          this.loadScript();
   //      });
  }

  ngAfterViewInit() {
  	this.loadjQuery();
  	this.loadScript();
  	 // this.loadAPI = new Promise((resolve) => {
    //         console.log('resolving promise...');
    //         this.loadScript();
    //     });
  	  
  }

ngAfterViewChecked (){
	//this.loadScript();
}
  public loadScript() {

  		for(var i=1; i<this.js.length; i++) {
  		console.log('preparing to load...')
        let node = document.createElement('script');
        node.src = 'assets/js/'+ this.js[i];
        node.type = 'text/javascript';
        node.async = true;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
  		}
        
    }

    public loadjQuery(): void {
       console.log('preparing to load...')
        let node = document.createElement('script');
        node.src = 'assets/js/jquery.js';
        node.type = 'text/javascript';
        node.async = true;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
    }

}
