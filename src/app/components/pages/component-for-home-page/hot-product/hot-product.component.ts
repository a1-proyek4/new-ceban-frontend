import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HotProductsService } from '../../../../services/hot-products.service';
import { MessageService} from '../../../../services/message.service';
import { Product} from '../../../../model/product';

@Component({
  selector: 'app-hot-product',
  templateUrl: './hot-product.component.html',
  styleUrls: ['./hot-product.component.css'],
  providers: [HotProductsService]
})
export class HotProductComponent implements OnInit,AfterViewInit {

  private hotProducts: Product[];
  private keyHotProducts="_hot_products_";
  constructor(private hotProductsService : HotProductsService) { }

  ngOnInit() {
    if(localStorage.getItem(this.keyHotProducts) === null || localStorage.getItem(this.keyHotProducts) === undefined) {
      this.getHotProducts();
    }else{
       console.log("FETCH FROM localStorage"+localStorage.getItem(this.keyHotProducts));
      this.hotProducts=JSON.parse(localStorage.getItem(this.keyHotProducts));
     // this.getHotProducts();
      console.log("ISI DATA"+this.hotProducts);
    }
  }

    ngAfterViewInit() {
     // this.getHotProducts();
    }
  getHotProducts(): void {
    console.log("FETCH FETCH SERVER");
    this.hotProductsService.getHotProducts()
                            .subscribe(hotProduct => {this.hotProducts=hotProduct; localStorage.setItem(this.keyHotProducts, JSON.stringify(hotProduct));}, err=> console.log(err));
  }
}
