import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/pages/component-for-home-page/home-page/home-page.component';
import { VerifyUserComponent } from './components/pages/component-for-home-page/verify-user/verify-user.component';
import { RegisterAccountComponent } from './components/pages/component-for-home-page/register-account/register-account.component';
import { RecoveryPasswordAccountComponent } from './components/pages/component-for-home-page/recovery-password-account/recovery-password-account.component';
import { CarouselPromoComponent } from './components/pages/component-for-home-page/carousel-promo/carousel-promo.component';
import { CategoryComponent } from './components/pages/component-for-home-page/category/category.component';
import { HotProductComponent } from './components/pages/component-for-home-page/hot-product/hot-product.component';
import { NewProductsComponent } from './components/pages/component-for-home-page/new-products/new-products.component';
import { FooterComponent } from './components/pages/component-for-home-page/footer/footer.component';
import { NavbarHomeComponent } from './components/pages/component-for-home-page/navbar-home/navbar-home.component';
import { CategoryPageComponent } from './components/pages/component-for-category-page/category-page/category-page.component';
import { NavbarCategoryComponent } from './components/pages/component-for-category-page/navbar-category/navbar-category.component';
import { DropdownCategoryComponent } from './components/pages/component-for-category-page/dropdown-category/dropdown-category.component';
import { ProductPageComponent } from './components/pages/component-for-product-page/product-page/product-page.component';
import { OthersProductComponent } from './components/pages/component-for-product-page/others-product/others-product.component';
import { CategoriesService } from './services/categories.service';
import { MessageService } from './services/message.service';
import { HotProductsService } from './services/hot-products.service';
import { NewProductsService} from './services/new-products.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    VerifyUserComponent,
    RegisterAccountComponent,
    RecoveryPasswordAccountComponent,
    CarouselPromoComponent,
    CategoryComponent,
    HotProductComponent,
    NewProductsComponent,
    FooterComponent,
    NavbarHomeComponent,
    CategoryPageComponent,
    NavbarCategoryComponent,
    DropdownCategoryComponent,
    ProductPageComponent,
    OthersProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [MessageService, HotProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
