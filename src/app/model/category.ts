export class Category {
	ctrId: string;
    ctrImage: string;
    ctrName: string;
    ctrDesc:string;
}