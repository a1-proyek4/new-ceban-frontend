import { Injectable }  from '@angular/core';
import { Headers, Http,  Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';
import {Category} from '../model/category';


@Injectable()
export class CategoriesService {

  private headers: Headers;
  private categoriesUrl = "http://ceban.azurewebsites.net/api/categories";
  private categoriesObserv: Observable<Category[]>;
  constructor(private http: Http) { 
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        
  }
  
  	getCategories(): Observable<Category[]> {
  		return this.http.get(this.categoriesUrl)
  										 .map(res=> this.extractData(res))
  										 .catch(this.handleError);
  }


   getCategoryById(id: number): Observable<Category> {
    return this.http.get(this.categoriesUrl+"/"+id).map(res=> this.extractData(res)).catch(this.handleError);
  }

  getSubCategoryById(id: number): Observable<Category[]> {
    return this.http.get(this.categoriesUrl+"/"+id+"/sub_categories").map(res=> this.extractData(res)).catch(this.handleError);
  }
  private extractData(res: Response) {
    let body = res.json();
    console.log(body);
    console.log(res);
    return body.data;
  }


  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.log(errMsg);
    return Observable.throw(errMsg);
  }

}
