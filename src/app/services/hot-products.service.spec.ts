import { TestBed, inject } from '@angular/core/testing';
import { HotProductsService } from './hot-products.service';

describe('HotProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HotProductsService]
    });
  });

  it('should ...', inject([HotProductsService], (service: HotProductsService) => {
    expect(service).toBeTruthy();
  }));
});
