import { TestBed, inject } from '@angular/core/testing';
import { NewProductsService } from './new-products.service';

describe('NewProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewProductsService]
    });
  });

  it('should ...', inject([NewProductsService], (service: NewProductsService) => {
    expect(service).toBeTruthy();
  }));
});
